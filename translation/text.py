import re

from typing import List

unusual_word_regex = re.compile('\w*\W+\w*')

def get_unusual_words(words: List[str]) -> List[str]:
    '''Get unusual words from a list of words. An anusual word is a word that
       contains at least one non-word character.

    :param: words List of words.
    :return: List of unusual words.
    '''
    unusual_words = []
    for word in words:
        match = unusual_word_regex.match(word)
        if match is not None:
            unusual_words.append(match.group())
    return unusual_words
