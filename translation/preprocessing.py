import numpy as np
import torch

from collections import Counter
from typing import List, Dict, Optional

from torch.utils import data

from .utils import get_all_words


def get_word_to_index(tokenized_texts: List[List[str]]) -> Dict[str, int]:
    '''Consume a list of tokenized texts and return a dictionary mapping from
       tokens to their indices.

    :param: tokenized_texts A list of lists of tokens.
    :return: A dictionary mapping from tokens to indices.
    '''
    flattened_texts = (word for words in tokenized_texts for word in words)
    word_counts = Counter(flattened_texts)
    word2index = {word_count[0]: i for i, word_count in
                  enumerate(word_counts.most_common(), 1)}
    return word2index


def get_index_to_word(word2index: Dict[str, int]) -> Dict[int, str]:
    '''Consume a dictionary maping from words to indices and return a dictionary
       mapping from indices to words.

    :param word2index: dict mapping from words to indices.
    :returns: dict mapping from indices to words.
    '''
    return {index: word for word, index in word2index.items()}


def replace_words_with_indices(texts: List[List[str]],
                               word2index: Dict[str, int]) -> List[List[int]]:
    '''Consume a list of tokenized texts and a mapping from word to index and
       return a list of list of integers representing words in the list of
       tokenized_texts.

    :param texts: A list of tokenized texts.
    :param word2index: A dictonary mapping from words to indices.
    :returns: A list of lists of integers representing words.
    '''
    return [[word2index[word] for word in text] for text in texts]


def texts_to_one_hot(texts_ind: List[List[int]], n_words: int):
    '''Convert a list of texts converted to list of indices to an array of
       one-hot vectors. Texts must be of the same size.

    :param texts_ind: A list of texts converted to list of indices.
    :param n_words: How many words there are in the vocabulary.
    :return: An array of texts converted to one-hot vectors.
    '''

    n_sentences = len(texts_ind)
    text_len = len(texts_ind[0])

    vectorized_words = np.zeros((n_sentences, text_len, n_words))

    for i, text in enumerate(texts_ind):
        for j, word in enumerate(text):
            vectorized_words[i, j, word] = 1

    return vectorized_words


def pad_texts(texts_ind: List[List[int]],
              text_len: Optional[int] = None) -> List[List[int]]:
    '''Padd text with zeros to given length and truncate too long sequences.

    :param texts_ind: Tokenized texts converted to indices.
    :returns: Tokenized texts padded or truncated if necessary.
    '''
    if text_len is None:
        text_len = max(len(text) for text in texts_ind)

    padded_texts = []
    for text in texts_ind:
        len_text = len(text)
        if len_text < text_len:
            text_padded = text + [0 for i in range(text_len - len_text)]
        elif len_text > text_len:
            text_padded = text[:text_len]
        else:
            text_padded = list(text)

        padded_texts.append(text_padded)
    return padded_texts


class TextsPreprocessor:

    def __call__(self, texts: List[str]):
        tokenized_texts = [text.split() for text in texts]

        if not hasattr(self, 'word2ind'):
            self.word2ind = get_word_to_index(tokenized_texts)
            self.ind2word = get_index_to_word(self.word2ind)

        tokenized_texts_ind = replace_words_with_indices(tokenized_texts,
                                                         self.word2ind)
        padded_texts = pad_texts(tokenized_texts_ind)
        one_hot_texts = texts_to_one_hot(padded_texts, len(self.word2ind) + 1)
        return one_hot_texts


class SentencesDataset(data.Dataset):
    '''A class responsible of preprocessing lists of sentences for source and
       target languages for machine translation and storing them in torch.Tensors.

    :param sentences_lang_in: Source language sentences.
    :param sentences_lang_out: Target language sentences.
    :raises: ValueError: When numbers of source and target sentences do not match.
    '''

    def __init__(self, sentences_lang_in: List[str], sentences_lang_out: List[str]):
        if len(sentences_lang_in) != len(sentences_lang_out):
            raise ValueError('Both datasets need to be of the same length')

        self.preprocessor_in = TextsPreprocessor()
        self.preprocessor_out = TextsPreprocessor()

        preprocessed_in = self.preprocessor_in(sentences_lang_in)
        preprocessed_out = self.preprocessor_out(sentences_lang_out)

        self.in_tensor = torch.from_numpy(preprocessed_in)

        out_seq_len = preprocessed_out.shape[1]
        out_n_words = preprocessed_out.shape[2]
        self.out_tensor = (torch.from_numpy(preprocessed_out)
                           .view(-1, out_seq_len, out_n_words)
                           .argmax(dim=-1))

    def __getitem__(self, index: int):
        x = self.in_tensor[index]
        y = self.out_tensor[index]
        return x, y

    def __len__(self):
        return self.in_tensor.shape[0]
