from typing import List, Dict

def read_texts(path: str) -> List[str]:
    '''Read sentences from a file to a list.

    :param: path Path to the file.
    :return: List of sentences.
    '''
    with open(path) as f:
        texts = f.readlines()
    return texts

def read_data(path: str) -> Dict[str, List[str]]:
    '''Read data files with French and English sentences from a given folder into
       a dictionary.

    :param: path Path to a folder with files with French and English sentences.
    :return: A dictionary with language as key and a list of sentences as a value.
    '''
    data_fr = read_texts(path + '/fr.txt')
    data_en = read_texts(path + '/en.txt')
    return {'French': data_fr, 'English': data_en}


def get_all_words(texts: List[str]) -> List[str]:
    '''Consume a list of strings and return a list of individual words in each
       string.

    :param: texts A list of sentences.
    :return: A list of individual words in each sentence in texts.
    '''
    return [word for text in texts for word in text.split()]
