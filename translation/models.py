import torch

from torch import nn

class EncoderDecoder(nn.Module):

    def __init__(self, input_size, hidden_size, out_seq_len, n_vocab_out):
        super().__init__()

        self.out_seq_len = out_seq_len
        self.n_vocab_out = n_vocab_out

        self.encoder = nn.GRU(input_size, hidden_size, batch_first=True)
        self.decoder = nn.GRU(hidden_size, hidden_size, batch_first=True)
        self.linear = nn.Linear(hidden_size, n_vocab_out)

    def forward(self, x):
        batch_size = x.size(0)

        _, hidden = self.encoder(x)
        hidden = hidden.view(batch_size, 1, -1).repeat(1, self.out_seq_len, 1)

        decoder_out, _ = self.decoder(hidden)

        linear_out = self.linear(decoder_out)

        out = torch.softmax(linear_out, dim=-1)

        return out
