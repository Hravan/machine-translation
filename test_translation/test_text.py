from translation.text import get_unusual_words

def test_get_unusual_words():
    words = ['chien', 'le', 'as-tu']
    assert get_unusual_words(words) == ['as-tu']
