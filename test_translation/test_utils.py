from translation.utils import *

def test_get_all_words():
    texts = ['I like cats', 'I like dogs']
    assert get_all_words(texts) == ['I', 'like', 'cats', 'I', 'like', 'dogs']
