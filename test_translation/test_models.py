import numpy as np
import torch

from translation.models import *

class TestEncoderDecoder:

    def test_one_hot_vectors(self):
        encoded_texts = np.array([[[0, 1, 0],
                                   [0, 0, 1],
                                   [1, 0, 0],
                                   [1, 0, 0]],
                                  [[0, 0, 1],
                                   [0, 1, 0],
                                   [0, 1, 0],
                                   [1, 0, 0]]], dtype=np.float32)
        encoded_texts_tensor = torch.from_numpy(encoded_texts)
        batch_size = 2
        input_size = 3
        hidden_size = 8
        out_seq_len = 3
        n_vocab_out = 3

        encoder_decoder = EncoderDecoder(input_size,
                                         hidden_size,
                                         out_seq_len,
                                         n_vocab_out)

        expected_out_shape = (batch_size, out_seq_len, n_vocab_out)
        out = encoder_decoder(encoded_texts_tensor)
        assert out.size() == expected_out_shape
