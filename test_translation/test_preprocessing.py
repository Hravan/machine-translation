import numpy as np
import pytest
import torch

from translation.preprocessing import *

class TestSentencesToOneHot:

    def test_sentences_same_size_to_one_hot(self):
        texts = [[1, 2, 4],
                 [3, 2, 3]]
        expected_out = np.array([[[0, 1, 0, 0, 0],
                                  [0, 0, 1, 0, 0],
                                  [0, 0, 0, 0, 1]],
                                 [[0, 0, 0, 1, 0],
                                  [0, 0, 1, 0, 0],
                                  [0, 0, 0, 1, 0]]])
        assert (texts_to_one_hot(texts, 5) == expected_out).all()

    def test_sentences_with_padding(self):
        texts = [[1, 2, 3],
                 [4, 5, 0]]
        expected_out = np.array([[[0, 1, 0, 0, 0, 0],
                                  [0, 0, 1, 0, 0, 0],
                                  [0, 0, 0, 1, 0, 0]],
                                 [[0, 0, 0, 0, 1, 0],
                                  [0, 0, 0, 0, 0, 1],
                                  [1, 0, 0, 0, 0, 0]]])
        assert (texts_to_one_hot(texts, 6) == expected_out).all()


class TestGetWord2Index:

    def test_simple_case(self):
        tokenized_texts = [['word1', 'word1', 'word2'],
                           ['word1', 'word3'],
                           ['word1', 'word3', 'word3']]
        expected_out = {'word1': 1, 'word3': 2, 'word2': 3}
        assert get_word_to_index(tokenized_texts) == expected_out


class TestGetIndex2Word:

    def test_simple_case(self):
        word2index = {'word1': 1, 'word3': 2, 'word2': 3}
        expected_out = {1: 'word1', 2: 'word3', 3: 'word2'}
        assert get_index_to_word(word2index) == expected_out


class TestReplaceWordWithIndex:

    def test_simple_case(self):
        word2index = {'word1': 1, 'word3': 2, 'word2': 3}
        tokenized_texts = [['word1', 'word1', 'word2'],
                           ['word1', 'word3'],
                           ['word1', 'word3', 'word3']]
        expected_out = [[1, 1, 3],
                        [1, 2],
                        [1, 2, 2]]
        assert (replace_words_with_indices(tokenized_texts, word2index)
                == expected_out)

class TestPadTexts:

    def test_texts_different_length_pad_to_longest_on_right(self):
        texts = [[1, 2, 3],
                 [4, 5]]
        expected_out = [[1, 2, 3],
                        [4, 5, 0]]
        assert pad_texts(texts) == expected_out

    def test_texts_same_length_no_padding(self):
        texts = [[1, 2],
                 [3, 4]]
        expected_out = list(texts)
        assert pad_texts(texts) == expected_out

    def test_padding_size_specified(self):
        texts = [[1, 2, 3],
                 [4, 5]]
        seq_len = 4
        expected_out = [[1, 2, 3, 0],
                        [4, 5, 0, 0]]
        assert pad_texts(texts, seq_len) == expected_out

    def test_padding_size_specified_too_long_text(self):
        texts = [[1, 2, 3, 4],
                 [1, 2]]
        text_len = 3
        expected_out = [[1, 2, 3],
                        [1, 2, 0]]
        assert pad_texts(texts, text_len) == expected_out

class TestTextPreprocessor:

    def test_preprocessing_pipeline_one_hot_encoding(self):
        texts = ['word1 word1 word2 word3',
                 'word1 word1 word3']
        preprocessor = TextsPreprocessor()

        expected_out = np.array([[[0, 1, 0, 0],
                                  [0, 1, 0, 0],
                                  [0, 0, 0, 1],
                                  [0, 0, 1, 0]],
                                 [[0, 1, 0, 0],
                                  [0, 1, 0, 0],
                                  [0, 0, 1, 0],
                                  [1, 0, 0, 0]]])

        one_hot_texts = preprocessor(texts)
        assert (one_hot_texts == expected_out).all()

        expected_word2ind = dict(word1=1, word3=2, word2=3)
        assert preprocessor.word2ind == expected_word2ind

        expected_ind2word = {1: 'word1', 2: 'word3', 3: 'word2'}
        assert preprocessor.ind2word == expected_ind2word

@pytest.fixture
def sentences_dataset():
    sentences_eng = ['a big lion',
                        'a small tiger']
    sentences_fr = ['un grand lion',
                       'un petit tigre']
    sent_dataset = SentencesDataset(sentences_eng, sentences_fr)
    yield sent_dataset


class TestSentencesDataset:

    def test_get_item(self, sentences_dataset):

        item_1_eng = torch.Tensor([[0, 1, 0, 0, 0, 0],
                                   [0, 0, 1, 0, 0, 0],
                                   [0, 0, 0, 1, 0, 0]]).double()
        item_1_fr = torch.Tensor([1, 2, 3]).long()

        out = sentences_dataset[0]
        assert  out[0].equal(item_1_eng)
        assert out[1].equal(item_1_fr)

    def test_len(self, sentences_dataset):
        assert len(sentences_dataset) == 2

    def test_datasets_different_lengths(self):
        sentences_eng = ['a big lion']
        sentences_fr = ['un grand lion',
                        'un petit tigre']
        with pytest.raises(ValueError) as exc_info:
            SentencesDataset(sentences_eng, sentences_fr)
        exc_message = 'Both datasets need to be of the same length'
        assert exc_info.match(exc_message)
